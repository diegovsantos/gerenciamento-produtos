<?php
require_once "src/config/configApp.php";
require_once "src/config/autoload.php";

use GerProd\Core\Core;

$template = file_get_contents("./views/template/master.php");

ob_start();
    $core = new Core();
    $core->start($_GET);

    $saida = ob_get_contents();
ob_end_clean();

$templateFull = str_replace('{{conteudo_dinamico}}', $saida, $template);

echo $templateFull;