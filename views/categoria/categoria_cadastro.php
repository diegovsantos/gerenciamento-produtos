<?php
/**
 * @var Categoria $categoria
 */

use GerProd\Models\Categoria\Categoria;

?>
<main class="content">
    <h1 class="title new-item">Categoria</h1>

    <form action="<?= (isset($categoria)) ? "?views=categorias/update/{$categoria->getCodigo()}" : "?views=categorias/insert"?>" method="post">
        <div class="input-field">
            <label for="category-name" class="label">Categoria:</label>
            <input type="text" id="category-name" class="input-text" name="categoria" required
                   value="<?= isset($categoria) ? $categoria->getCategoria() : null ?>" />

        </div>
        <?php if (isset($categoria)): ?>
            <div class="input-field">
                <label for="category-code" class="label">Código:</label>
                <input type="text" id="category-code" class="input-text"
                       value="<?= $categoria->getCodigo() ?>" readonly style='background: lightgray;'/>
            </div>
        <?php endif ?>
        <div class="actions-form">
            <a href="?views=categorias" class="action back">Voltar</a>
            <input class="btn-submit btn-action"  type="submit" value="<?= (isset($categoria)) ? "Editar Categoria" : "Cadastrar Categoria"?>" />
        </div>
    </form>
</main>