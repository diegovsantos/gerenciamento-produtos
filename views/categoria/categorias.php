<?php
/**
 * @var array $categorias
 * @var Categoria $categoria
 */

use GerProd\Models\Categoria\Categoria;
?>
<div class="header-list-page">
    <h1 class="title">Categorias</h1>
    <?php if (isset($erro)): ?>
        <h4 class="title"><?= $erro ?></h4>
    <?php endif ?>
    <a href="?views=categorias/cadastrar" class="btn-action">Adicionar Nova Categoria</a>
</div>
<table class="data-grid">
    <tr class="data-row">
        <th class="data-grid-th">
            <span class="data-grid-cell-content">Código</span>
        </th>
        <th class="data-grid-th">
            <span class="data-grid-cell-content">Categoria</span>
        </th>
        <th class="data-grid-th">
            <span class="data-grid-cell-content">Ações</span>
        </th>
    </tr>
    <?php foreach ($categorias as $categoria): ?>
        <tr class="data-row">
            <td class="data-grid-td">
                <span class="data-grid-cell-content"><?= $categoria->getCodigo() ?></span>
            </td>
            <td class="data-grid-td">
                <span class="data-grid-cell-content"><?= $categoria->getCategoria() ?></span>
            </td>
            <td class="data-grid-td">
                <div class="actions">
                    <div class="action edit"><a href="?views=categorias/editar/<?= $categoria->getCodigo() ?>"><span>Editar</span></a></div>
                    <div class="action delete"><a href="?views=categorias/delete/<?= $categoria->getCodigo() ?>"><span>Remover</span></a></div>
                </div>
            </td>
        </tr>
    <?php endforeach ?>
</table>