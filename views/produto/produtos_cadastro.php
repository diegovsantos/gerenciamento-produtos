<?php
/**
 * @var Produto $produto
 * @var array $categorias
 * @var Categoria $categoria
 * @var array $categorias_ids
 */

use GerProd\Models\Categoria\Categoria;
use GerProd\Models\Produto\Produto;


?>
<main class="content">
    <h1 class="title new-item">Novo Produto</h1>
    <?php if (isset($erro)): ?>
        <h4 class="title new-item"><?= $erro ?></h4>
    <?php endif ?>

    <form action="<?= (isset($produto)) ? "?views=produtos/update" : "?views=produtos/insert" ?>" method="post" enctype="multipart/form-data">
        <div class="input-field">
            <label for="sku" class="label">Produto SKU</label>
            <input type="text" id="sku" name="sku" class="input-text" required <?= (isset($produto)) ? "readonly style=' background: lightgray;'" : "" ?>
                   pattern="[0-9]{4}-[0-9]{3}" placeholder="0000-000" title="SKU deve ser no formato 0000-000"
                   value="<?= (isset($produto)) ? $produto->getSku() : null?>" />
        </div>
        <div class="input-field">
            <label for="name" class="label">Nome do Produto:</label>
            <input type="text" id="name" name="nome" class="input-text" required
                   value="<?= (isset($produto)) ? $produto->getNome() : null?>" />
        </div>
        <div class="input-field">
            <label for="price" class="label">Preço:</label>
            <input type="number" min="0" step=".01" id="price" name="preco" class="input-text" required
                   value="<?= (isset($produto)) ? $produto->getPreco() : null?>" />
        </div>
        <div class="input-field">
            <label for="quantity" class="label">Quantidade:</label>
            <input type="number" id="quantity" name="quantidade" class="input-text" required
                   value="<?= (isset($produto)) ? $produto->getQuantidade() : null?>" />
        </div>
        <div class="input-field">
            <label for="category" class="label">Categorias: </label>
            <select multiple id="category" class="input-text" name="categorias[]">
                <?php if (count($categorias)): ?>
                    <?php foreach ($categorias as $categoria): ?>
                        <option value="<?= $categoria->getCodigo() ?>" <?= isset($categorias_ids) ? in_array($categoria->getCodigo(), $categorias_ids) ? "selected" : "" : "" ?>><?= $categoria->getCategoria() ?></option>
                    <?php endforeach; ?>
                <?php else: ?>
                    <option value="">Sem Categorias Cadastradas</option>
                <?php endif ?>
            </select>
        </div>
        <div class="input-field">
            <label for="description" class="label">Descrição: </label>
            <textarea id="description" class="input-text" name="descricao" required><?= (isset($produto)) ? $produto->getDescricao() : null?></textarea>
        </div>
        <div class="input-field">
            <label for="image" class="label">Imagem:</label>
            <input type="file" name="imagem" accept="image/*">
        </div>
        <?php if (isset($produto) && $produto->getImagem() != ""): ?>
            <div class="input-field">
                <label for="image" class="label">Imagem Enviada:</label>
                <input type="hidden" name="imagem_endereco" value="<?= $produto->getImagem() ?>">
                <img src="<?= $produto->getImagem() ?>" width="164" height="145"  alt=""/>
            </div>
        <?php endif ?>
        <div class="actions-form">
            <a href="?views=produtos" class="action back">Voltar</a>
            <input class="btn-submit btn-action" type="submit" value="Cadastrar Produto" />
        </div>

    </form>
</main>