<?php
/**
 * @var array $produtos
 * @var Produto $produto
 */

use GerProd\Models\Produto\Produto;

?>
<div class="header-list-page">
    <h1 class="title">Produtos</h1>
    <a href="?views=produtos/cadastrar" class="btn-action">Adicionar novo produto</a>
</div>

<table class="data-grid">
    <tr class="data-row">
        <th class="data-grid-th">
            <span class="data-grid-cell-content">Nome</span>
        </th>
        <th class="data-grid-th">
            <span class="data-grid-cell-content">SKU</span>
        </th>
        <th class="data-grid-th">
            <span class="data-grid-cell-content">Preço</span>
        </th>
        <th class="data-grid-th">
            <span class="data-grid-cell-content">Quantidade</span>
        </th>
        <th class="data-grid-th">
            <span class="data-grid-cell-content">Categorias</span>
        </th>

        <th class="data-grid-th">
            <span class="data-grid-cell-content">Ações</span>
        </th>
    </tr>
    <?php if (count($produtos)): ?>
        <?php foreach ($produtos as $produto): ?>
            <tr class="data-row">
                <td class="data-grid-td">
                    <span class="data-grid-cell-content"><?= $produto->getNome() ?></span>
                </td>

                <td class="data-grid-td">
                    <span class="data-grid-cell-content"><?= $produto->getSku() ?></span>
                </td>

                <td class="data-grid-td">
                    <span class="data-grid-cell-content">R$ <?= $produto->getPrecoBr() ?></span>
                </td>

                <td class="data-grid-td">
                    <span class="data-grid-cell-content"><?= $produto->getQuantidade() ?></span>
                </td>

                <td class="data-grid-td">
                    <span class="data-grid-cell-content">
                        <?php
                            if (count($produto->getCategorias())) {
                                $categorias = [];
                                foreach ($produto->getCategorias() as $categoria) {
                                    $categorias[] = $categoria->getCategoria();
                                }
                                echo implode(", ", $categorias);
                            }
                        ?>
                    </span>
                </td>

                <td class="data-grid-td">
                    <div class="actions">
                        <div class="action edit"><a href="?views=produtos/editar/<?= $produto->getSku() ?>"><span>Editar</span></a></div>
                        <div class="action delete"><a href="?views=produtos/delete/<?= $produto->getSku() ?>"><span>Remover</span></a></div>
                    </div>
                </td>
            </tr>
        <?php endforeach ?>
    <?php else: ?>
        <tr class="data-row" align="center">
            <td colspan="6">
                <span>Não há produtos cadastrados</span>
            </td>
        </tr>
    <?php endif; ?>
</table>