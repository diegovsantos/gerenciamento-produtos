<?php
/**
 * @var array $produtos
 * @var Produto $produto
 */

use GerProd\Models\Produto\Produto;

?>
<main class="content">
    <div class="header-list-page">
        <h1 class="title">Dashboard</h1>
    </div>
    <div class="infor">
        Você tem <?= count($produtos) ?> cadastrados: <a href="?views=produtos/cadastrar" class="btn-action">Adicionar novo produto</a>
    </div>
    <ul class="product-list">
        <?php foreach ($produtos as $produto): ?>
            <li>
                <div class="product-image">
                    <img src="<?= $produto->getImagem() == "" ? "./views/template/assets/images/product/sem-imagem.png" : $produto->getImagem() ?>" layout="responsive"
                         width="164" height="145"
                         alt="<?= $produto->getNome() ?>"/>
                </div>
                <div class="product-info">
                    <div class="product-name"><span><?= $produto->getNome() ?></span></div>
                    <div class="product-price"><span class="special-price"><?= $produto->getQuantidade() ?> disponíveis</span> <span>R$<?= $produto->getPrecoBr() ?></span>
                    </div>
                </div>
            </li>
        <?php endforeach ?>
    </ul>
</main>