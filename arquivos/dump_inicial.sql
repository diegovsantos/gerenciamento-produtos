-- --------------------------------------------------------
-- Servidor:                     127.0.0.1
-- Versão do servidor:           10.4.11-MariaDB - mariadb.org binary distribution
-- OS do Servidor:               Win64
-- HeidiSQL Versão:              11.0.0.5919
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;


-- Copiando estrutura do banco de dados para gerprod
CREATE DATABASE IF NOT EXISTS `gerprod` /*!40100 DEFAULT CHARACTER SET utf8 */;
USE `gerprod`;

-- Copiando estrutura para tabela gerprod.categorias
CREATE TABLE IF NOT EXISTS `categorias` (
  `codigo` int(11) NOT NULL AUTO_INCREMENT,
  `categoria` varchar(30) NOT NULL,
  PRIMARY KEY (`codigo`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

-- Copiando dados para a tabela gerprod.categorias: ~4 rows (aproximadamente)
/*!40000 ALTER TABLE `categorias` DISABLE KEYS */;
INSERT IGNORE INTO `categorias` (`codigo`, `categoria`) VALUES
	(1, 'Tênis'),
	(2, 'Mochila'),
	(3, 'Mouse'),
	(4, 'Teclado');
/*!40000 ALTER TABLE `categorias` ENABLE KEYS */;

-- Copiando estrutura para tabela gerprod.categoria_produto
CREATE TABLE IF NOT EXISTS `categoria_produto` (
  `categoria_codigo` int(11) NOT NULL,
  `produto_sku` varchar(15) NOT NULL,
  KEY `categorias_categoria_produto_fk_idx` (`categoria_codigo`),
  KEY `produtos_categoria_produto_fk_idx` (`produto_sku`),
  CONSTRAINT `categorias_categoria_produto_fk` FOREIGN KEY (`categoria_codigo`) REFERENCES `categorias` (`codigo`),
  CONSTRAINT `produtos_categoria_produto_fk` FOREIGN KEY (`produto_sku`) REFERENCES `produtos` (`sku`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Copiando dados para a tabela gerprod.categoria_produto: ~0 rows (aproximadamente)
/*!40000 ALTER TABLE `categoria_produto` DISABLE KEYS */;
INSERT IGNORE INTO `categoria_produto` (`categoria_codigo`, `produto_sku`) VALUES
	(1, '0000-001'),
	(1, '0000-002'),
	(4, '0000-003'),
	(3, '0000-004'),
	(3, '0000-005'),
	(4, '0000-005'),
	(1, '0000-006'),
	(2, '0000-006'),
	(3, '0000-006'),
	(4, '0000-006');
/*!40000 ALTER TABLE `categoria_produto` ENABLE KEYS */;

-- Copiando estrutura para tabela gerprod.logs
CREATE TABLE IF NOT EXISTS `logs` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `log` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Copiando dados para a tabela gerprod.logs: ~0 rows (aproximadamente)
/*!40000 ALTER TABLE `logs` DISABLE KEYS */;
/*!40000 ALTER TABLE `logs` ENABLE KEYS */;

-- Copiando estrutura para tabela gerprod.produtos
CREATE TABLE IF NOT EXISTS `produtos` (
  `sku` varchar(15) NOT NULL,
  `nome` varchar(45) NOT NULL,
  `descricao` varchar(45) NOT NULL,
  `quantidade` int(11) NOT NULL,
  `preco` float(6,2) NOT NULL,
  `imagem` varchar(100) DEFAULT '',
  PRIMARY KEY (`sku`),
  UNIQUE KEY `sku_UNIQUE` (`sku`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Copiando dados para a tabela gerprod.produtos: ~1 rows (aproximadamente)
/*!40000 ALTER TABLE `produtos` DISABLE KEYS */;
INSERT IGNORE INTO `produtos` (`sku`, `nome`, `descricao`, `quantidade`, `preco`, `imagem`) VALUES
	('0000-001', 'Tênis Adidas Coreracer Masculino', 'Tênis Adidas Coreracer Masculino Preto com li', 10, 249.99, './uploads/20220121235252_tenis_adidas_coreracer.jpg'),
	('0000-002', 'Tênis Nike Downshifter 11 Feminino', 'Tênis Nike Downshifter 11 Feminino Preto com ', 7, 329.99, './uploads/20220121235506_tenis_nike_downshifter.jpg'),
	('0000-003', 'Teclado Gamer Redragon Shiva RGB, K512RGB PT', 'Sem economizar na iluminação, o Shiva traz um', 1, 299.86, './uploads/20220122000315_teclado_gamer_redragon_shiva.jpg'),
	('0000-004', 'Mouse Redragon Cobra M711', 'Sensor PIXART PMW3325 para Alta Performance (', 15, 187.50, './uploads/20220122000619_mouse_redragon_cobra.jpg'),
	('0000-005', 'Kit Gamer Teclado LED + Mouse KB013', 'Teclado e mouse gamer excelente para o uso no', 5, 269.90, './uploads/20220122000808_kit_gamer_teclado_mouse.jpg'),
	('0000-006', 'Cadastro de Produto sem Imagem', 'Apenas para apresentar todas as categorias do', 1, 0.99, '');
/*!40000 ALTER TABLE `produtos` ENABLE KEYS */;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
