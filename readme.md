# Sobre o sistema
Este sistema foi desenvolvido para gerenciar produtos, podendo cadastrar, editar, remover e atribuir categorias aos mesmo.

Para as categorias, também é possível cadastrar, editar e remover

O desenvolvimento do projeto seguiu os padrões MVC.

A modelagem do banco de dados foi feita através do software MySQL Workbench e pode ser visualizada no arquivo `arquivos/modelagem.mwb`

O manual de utilização do sistema pode ser visualizado no arquivo `arquivos/Manual de Utilização do Sistema.pdf`

# Instruções para instalação
- Servidor apache e MySql instalados
- executar arquivo `arquivos/dump_inicial.sql` para importação do banco de dados
- No arquivo `src/config/configApp.php`, deve-se configurar os dados de acesso ao banco de dados, atribuindo valores as constante, onde:
  - **SERVER**: Endereço do servidor MySql
  - **USER**: Usuário do banco de dados
  - **PASS**: Senha de acesso

# Tecnologias e Metodologias
- PHP 7.4
- MySQL 5.6.30
- psr-4
- Padrão MVC

