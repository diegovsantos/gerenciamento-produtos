<?php

namespace GerProd\Models\Produto;

use GerProd\Core\Functions;
use GerProd\Models\Categoria\Categoria;
use GerProd\Models\Database;

class Produto extends Functions
{
    private string $sku;
    private string $nome;
    private string $descricao;
    private int $quantidade;
    private float $preco;
    private string $imagem;
    private array $categorias;

    public function getSku(): string
    {
        return $this->sku;
    }

    public function setSku(string $sku): void
    {
        $this->sku = $sku;
    }

    public function getNome(): string
    {
        return $this->nome;
    }

    public function setNome(string $nome): void
    {
        $this->nome = $nome;
    }

    public function getDescricao(): string
    {
        return $this->descricao;
    }

    public function setDescricao(string $descricao): void
    {
        $this->descricao = $descricao;
    }

    public function getQuantidade(): int
    {
        return $this->quantidade;
    }

    public function setQuantidade(int $quantidade): void
    {
        $this->quantidade = $quantidade;
    }

    public function getPreco(): float
    {
        return $this->preco;
    }

    public function getPrecoBr(): string
    {
        return number_format($this->preco, "2", ",", ".");
    }

    public function setPreco(float $preco): void
    {
        $this->preco = $preco;
    }

    public function getImagem(): string
    {
        return $this->imagem;
    }

    public function setImagem(string $imagem): void
    {
        $this->imagem = $imagem;
    }

    public function getCategorias(): array
    {
        return $this->categorias;
    }

    public function setCategorias(array $categorias): void
    {
        $this->categorias = $categorias;
    }

    public static function recuperaProdutos(): array
    {
        $res = [];
        $query = Database::select("produtos");
        while ($row = $query->fetchObject(self::class)) {
            $row->categorias = Categoria::recuperaCategoriasPorProduto($row->sku);
            $res[] = $row;
        }
        return $res;
    }

    public static function recuperaProduto(string $sku): Produto
    {
        $produto = Database::select("produtos", ["sku = '$sku'"])->fetchObject(self::class);
        $produto->categorias = Categoria::recuperaCategoriasPorProduto($produto->sku);
        return $produto;
    }

    public static function insert($post): bool
    {
        $dados['sku'] = $post['sku'] != "" ? $post['sku'] : false;
        $dados['nome'] = $post['nome'] != "" ? $post['nome'] : false;
        $dados['descricao'] = $post['descricao'] != "" ? $post['descricao'] : false;
        $dados['quantidade'] = $post['quantidade'] != "" ? $post['quantidade'] : false;
        $dados['preco'] = $post['preco'] != "" ? $post['preco'] : false;

        $imagem = Functions::uploadArquivos($_FILES, 8);
        if ($imagem) {
            $dados['imagem'] = $imagem;
        } else {
            $dados['imagem'] = "";
        }

        try {
            $insert = Database::insert('produtos', $dados);
        } catch (\Exception $e) {
            return false;
        }
        if ($insert) {
            if (isset($post['categorias'])) {
                $categorias['produto_sku'] = $dados['sku'];

                foreach ($post['categorias'] as $categoria) {
                    $categorias['categoria_codigo'] = $categoria;
                    try {
                        Database::insert('categoria_produto', $categorias);
                    } catch (\Exception $e) {
                        return false;
                    }
                }
            }

            return true;
        } else {
            return false;
        }
    }

    public static function update($post): bool
    {
        $sku = $post['sku'] != "" ? $post['sku'] : false;
        $dados['nome'] = $post['nome'] != "" ? $post['nome'] : false;
        $dados['descricao'] = $post['descricao'] != "" ? $post['descricao'] : false;
        $dados['quantidade'] = $post['quantidade'] != "" ? $post['quantidade'] : false;
        $dados['preco'] = $post['preco'] != "" ? $post['preco'] : false;

        if (!empty($_FILES)) {
            $imagem = Functions::uploadArquivos($_FILES, 8);
            if ($imagem) {
                $dados['imagem'] = $imagem;
            }
        } else {
            if (!isset($post['imagem_endereco'])){
                $dados['imagem'] = "";
            }
        }

        $update = Database::update('produtos', $dados, "sku", $sku);
        if ($update) {
            Database::delete('categoria_produto', ['produto_sku' => $sku]);
            if (isset($post['categorias'])) {
                $categorias['produto_sku'] = $sku;

                foreach ($post['categorias'] as $categoria) {
                    $categorias['categoria_codigo'] = $categoria;
                    Database::insert('categoria_produto', $categorias);
                }
            }

            return true;
        } else {
            return false;
        }
    }

    public static function delete($sku): bool
    {
        $delete = Database::delete('produtos', ['sku' => $sku]);
        if ($delete) {
            return true;
        } else {
            return false;
        }
    }
}