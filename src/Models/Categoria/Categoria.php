<?php

namespace GerProd\Models\Categoria;

use GerProd\Models\Database;

class Categoria
{
    private int $codigo;
    private string $categoria;

    public function getCodigo(): string
    {
        return $this->codigo;
    }

    public function setCodigo(int $codigo): void
    {
        $this->codigo = $codigo;
    }

    public function getCategoria(): string
    {
        return $this->categoria;
    }

    public function setCategoria(string $categoria): void
    {
        $this->categoria = $categoria;
    }

    public static function insert($post): bool
    {
        $dados['categoria'] = $post['categoria'] != "" ? $post['categoria'] : false;

        $insert = Database::insert('categorias', $dados);
        if ($insert) {
            return true;
        } else {
            return false;
        }
    }

    public static function update($codigo, $post)
    {
        $dados['categoria'] = $post['categoria'] != "" ? $post['categoria'] : false;

        $update = Database::update('categorias', $dados, "codigo", $codigo);
        if ($update) {
            return true;
        } else {
            return false;
        }
    }

    public static function delete($codigo)
    {
        $dados['codigo'] = $codigo;
        $delete = Database::delete('categorias', $dados);
        if ($delete) {
            return true;
        } else {
            return false;
        }
    }

    public static function recuperaCategoria($codigo)
    {
        return Database::select('categorias', ["codigo = '$codigo'"])->fetchObject(self::class);
    }

    public static function recuperaCategorias(): array
    {
        $res = [];
        $query = Database::select("categorias");
        while ($row = $query->fetchObject(self::class)) {
            $res[] = $row;
        }

        return $res;
    }

    public static function recuperaCategoriasPorProduto(string $sku): array
    {
        $res = [];
        $query = Database::select('categoria_produto', ["produto_sku = '$sku'"]);
        while ($row = $query->fetchObject()) {
            $res[] = self::recuperaCategoria($row->categoria_codigo);
        }

        return $res;
    }
}