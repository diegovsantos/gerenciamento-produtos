<?php

namespace GerProd\Core;

abstract class Functions
{
    protected static string $uploaddir = "./uploads/";

    public static function uploadArquivos($arquivos, $tamanhoMaximo)
    {
        foreach ($arquivos as $key => $arquivo) {
            $erros[$key]['bol'] = false;
            if ($arquivo['error'] != 4) {
                $nomeArquivo = $arquivo['name'];
                $tamanhoArquivo = $arquivo['size'];
                $arquivoTemp = $arquivo['tmp_name'];
                $explode = explode('.', $nomeArquivo);

                $novoNome = date('YmdHis')."_".$nomeArquivo;
                $maximoPermitido = $tamanhoMaximo*1048576;

                if ($tamanhoArquivo <= $maximoPermitido) {
                    if (move_uploaded_file($arquivoTemp, self::$uploaddir . $novoNome)) {
                        return self::$uploaddir . $novoNome;
                    } else {
                        return false;
                    }
                } else {
                    return false;
                }
            }
        }
        return false;
    }
}