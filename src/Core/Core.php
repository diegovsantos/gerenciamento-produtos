<?php
namespace GerProd\Core;

class Core
{
    public function start($urlGet)
    {
        $controller = "GerProd\\Controllers\\";
        if (isset($urlGet['views'])) {
            $views = explode("/", $urlGet['views']);
            $controller .= ucfirst($views[0]) . 'Controller';
        } else {
            $controller .= "DashboardController";
        }

        if (!class_exists($controller)){
            $controller = "GerProd\\Controllers\\ErroController";
        }
        $acao = $views[1] ?? "index";

        if (method_exists($controller, $acao)) {
            call_user_func([new $controller, $acao], $views[2] ?? null);
        } else {
            call_user_func([new $controller, "index"], []);
        }

    }
}