<?php

namespace GerProd\Controllers;

use GerProd\Models\Produto\Produto;

class DashboardController
{
    public function index()
    {
        $produtos = Produto::recuperaProdutos();
        include "./views/dashboard.php";
    }
}