<?php

namespace GerProd\Controllers;

use GerProd\Models\Categoria\Categoria;
use GerProd\Models\Produto\Produto;

class ProdutosController
{
    public function index()
    {
        $produtos = Produto::recuperaProdutos();
        include "views/produto/produtos.php";
    }

    public function cadastrar($error = false)
    {
        if ($error) {
            $erro = $error;
        }
        $categorias = Categoria::recuperaCategorias();
        include "views/produto/produtos_cadastro.php";
    }

    public function insert()
    {
        if (Produto::insert($_POST)) {
            header('Location: ?views=produtos');
        } else {
            $erro = "Falha ao inserir o produto";
            $this->cadastrar($erro);
        }
    }

    public function editar(string $sku)
    {
        $produto = Produto::recuperaProduto($sku);
        $categorias = Categoria::recuperaCategorias();
        if (count($produto->getCategorias())) {
            foreach ($produto->getCategorias() as $categoria) {
                $categorias_ids[] = $categoria->getCodigo();
            }
        }
        include "views/produto/produtos_cadastro.php";
    }

    public function update()
    {
        if (Produto::update($_POST)) {
            header('Location: ?views=produtos');
        } else {
            echo "error insert";
        }
    }

    public function delete(string $sku)
    {
        if (Produto::delete($sku)) {
            header('Location: ?views=produtos');
        } else {
            echo "error delete";
        }
    }
}