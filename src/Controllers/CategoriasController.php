<?php

namespace GerProd\Controllers;

use GerProd\Models\Categoria\Categoria;

class CategoriasController
{
    public function index()
    {
        $categorias = Categoria::recuperaCategorias();
        include "views/categoria/categorias.php";
    }

    public function cadastrar()
    {
        include "views/categoria/categoria_cadastro.php";
    }

    public function insert()
    {
        if (Categoria::insert($_POST)) {
            header('Location: ?views=categorias');
        } else {
            $erro = "Falha ao inserir categoria";
            $this->cadastrar();
        }
    }

    public function editar($codigo)
    {
        $categoria = Categoria::recuperaCategoria($codigo);
        include "views/categoria/categoria_cadastro.php";
    }

    public function update($codigo)
    {
        if (Categoria::update($codigo, $_POST)) {
            header('Location: ?views=categorias');
        } else {
            $erro = "Falha ao atualizar a Categoria";
            $this->editar($codigo);
        }
    }

    public function delete($codigo)
    {
        if (Categoria::delete($codigo)) {
            header('Location: ?views=categorias');
        } else {
            $erro = "Falha ao deletar Categoria";
            $this->index();
        }
    }
}